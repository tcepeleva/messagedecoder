﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course
{
    class Coding
    {
        private static string alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
        private static char[] characters = alphabet.ToLower().ToCharArray();
        private static int N = characters.Length;
        internal static string Decode(string input, string keyword)
        {
            keyword = keyword.ToLower();

            string result = "";

            int keyword_index = 0;

            foreach (char symbol in input)
            {
                if (characters.Contains(char.ToLower(symbol)))
                {
                    int p = (Array.IndexOf(characters, char.ToLower(symbol)) + N - Array.IndexOf(characters, keyword[keyword_index])) % N;

                    //если буква была большой:
                    if (char.IsUpper(symbol))
                        result += char.ToUpper(characters[p]);
                    else
                        result += characters[p];

                    keyword_index++;

                    if (keyword_index == keyword.Length)
                        keyword_index = 0;
                }
                else
                {
                    result += symbol;
                }
            }

            return result;
        }

        internal static string Encode(string input, string keyword)
        {
            keyword = keyword.ToLower();

            string result = "";

            int keyword_index = 0;

            foreach (char symbol in input)
            {
                if (characters.Contains(char.ToLower(symbol)))
                {
                    int c = (Array.IndexOf(characters, char.ToLower(symbol)) + Array.IndexOf(characters, keyword[keyword_index])) % N;

                    //если буква была большой:
                    if (char.IsUpper(symbol))
                        result += char.ToUpper(characters[c]);
                    else
                        result += characters[c];

                    keyword_index++;

                    if (keyword_index == keyword.Length)
                        keyword_index = 0;
                }
                else
                {
                    result += symbol;
                }
            }

            return result;
        }

    }
}
