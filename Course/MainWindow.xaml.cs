﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using Word = Microsoft.Office.Interop.Word;

namespace Course
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        

        public MainWindow()
        {
            InitializeComponent();
        }


        private string OpenWord(Object filename)
        {
            Object confirmConversions = Type.Missing;
            Object readOnly = Type.Missing;
            Object addToRecentFiles = Type.Missing;
            Object passwordDocument = Type.Missing;
            Object passwordTemplate = Type.Missing;
            Object revert = Type.Missing;
            Object writePasswordDocument = Type.Missing;
            Object writePasswordTemplate = Type.Missing;
            Object format = Type.Missing;
            Object encoding = Type.Missing;
            Object visible = Type.Missing;
            Object openConflictDocument = Type.Missing;
            Object openAndRepair = Type.Missing;
            Object documentDirection = Type.Missing;
            Object noEncodingDialog = Type.Missing;
            Word.Application Progr = new Microsoft.Office.Interop.Word.Application();
            Progr.Documents.Open(ref filename,
                ref confirmConversions,
                ref readOnly,
                ref addToRecentFiles,
                ref passwordDocument,
                ref passwordTemplate,
                ref revert,
                ref writePasswordDocument,
                ref writePasswordTemplate,
                ref format,
                ref encoding,
                ref visible,
                ref openConflictDocument,
                ref openAndRepair,
                ref documentDirection,
                ref noEncodingDialog);
            Word.Document Doc = new Microsoft.Office.Interop.Word.Document();
            Doc = Progr.Documents.Application.ActiveDocument;
            object start = 0;
            object stop = Doc.Characters.Count;
            Word.Range Rng = Doc.Range(ref start, ref stop);
            string Result = Rng.Text;
            object sch = Type.Missing;
            object aq = Type.Missing;
            object ab = Type.Missing;
            Progr.Quit(ref sch, ref aq, ref ab);
            return Result;
        }

        private void btnDownloadFile_Click(object sender, RoutedEventArgs e)
        {

            //диалоговое окно:
            Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog();

            fileDialog.Filter = "Text documents (*.txt,*.docx)|*.txt;*.docx|Text files (*.txt)|*.txt|Word documents (*.docx)|*.docx|All files (*.*)|*.*";

            if (fileDialog.ShowDialog() == true)
            {
                try
                {
                    txtBlockOrig.Text = "";
                    if (fileDialog.FileName.Contains(".txt"))
                        txtBlockOrig.Text = File.ReadAllText(fileDialog.FileName, Encoding.Default);
                    else if (fileDialog.FileName.Contains(".docx"))
                        txtBlockOrig.Text = OpenWord(fileDialog.FileName);
                }
                catch
                {
                    MessageBox.Show("Невозможно открыть файл", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        
        private void btnDecipherText_Click(object sender, RoutedEventArgs e)
        {
            txtBlockChanged.Text = Coding.Decode(txtBlockOrig.Text, txtKeyword.Text);
            lblChanged.Content = "Расшифрованный текст:";
        }

        private void btnCipherText_Click(object sender, RoutedEventArgs e)
        {
            txtBlockChanged.Text = Coding.Encode(txtBlockOrig.Text, txtKeyword.Text);
            lblChanged.Content = "Зашифрованный текст:";
        }

        private void btnReverse_Click(object sender, RoutedEventArgs e)
        {
            string temp = txtBlockChanged.Text;
            txtBlockChanged.Text = txtBlockOrig.Text;
            txtBlockOrig.Text = temp;
            if (lblChanged.Content.Equals("")) { }
                
            else if (lblChanged.Content.Equals("Зашифрованный текст:"))
                lblChanged.Content = "Расшифрованный текст:";
            else
                lblChanged.Content = "Зашифрованный текст:";
        }

        private void btnSaveFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog fileDialog = new Microsoft.Win32.SaveFileDialog();

            fileDialog.Filter = "Text files (*.txt)|*.txt|Word documents (*.docx)|*.docx|All files (*.*)|*.*";


            if (fileDialog.ShowDialog() == true)
            {
                try
                {
                    if (fileDialog.FileName.Contains(".txt"))
                    {
                        File.WriteAllText(fileDialog.FileName, txtBlockChanged.Text, Encoding.Default);
                        MessageBox.Show("Файл успешно сохранён!", "Save Message", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else if (fileDialog.FileName.Contains(".docx"))
                    {
                        try
                        {
                            File.Create(fileDialog.FileName);

                            Word.Application wordApp = new Word.Application();
                            wordApp.Application.Documents.Add();
                            object start = 0;
                            object end = 0;

                            Word.Range rng = wordApp.Application.ActiveDocument.Range(ref start, ref end);
                            rng.Text = txtBlockChanged.Text;
                            wordApp.Documents.Save();
                            wordApp.Documents.Close();
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }

                }
                catch
                {
                    MessageBox.Show("Не удалось сохранить файл", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
